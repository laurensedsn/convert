package libconvert

import (
	"errors"
	"math"
)

// Private: Converts between metric values
func metrConv(d Data) (float64, error) {
	if d.Conv != "metric" {
		return 0, errors.New(invalidConv)
	}

	_, err := determineType(d)

	if err != nil {
		return 0, err
	}

	m := map[string]int{
		"km": 7,
		"m":  4,
		"cm": 2,
		"mm": 1,
	}

	// Calculate distance between two units using
	// mm as a baseline. Then multiply/divide as needed
	switch {
	case m[d.Unit] == m[d.Target]:
		return d.Value, nil
	case m[d.Unit] < m[d.Target]:
		return d.Value / math.Pow10(int(m[d.Target]-m[d.Unit])), nil
	case m[d.Unit] > m[d.Target]:
		return d.Value * math.Pow10(int(m[d.Unit]-m[d.Target])), nil
	default:
		return 0, errors.New(invalidConv)
	}

}

// Private: Converts between Imperial values
func imperConv(d Data) (float64, error) {
	if d.Conv != "imperial" {
		return 0, errors.New(invalidConv)
	}

	_, err := determineType(d)

	if err != nil {
		return 0, err
	}

	type sub struct {
		pos   int
		delta float64
	}
	m := map[string]sub{
		"mi": {3, 1760},
		"yd": {2, 3},
		"ft": {1, 12},
		"in": {0, 1},
	}
	modifier := 1.0

	// Formula: inch * 12 * 3 * 1760 = mile
	// Finds the position difference between the
	// two units and returns the calculation
	switch {
	case d.Target == d.Unit:
		return d.Value, nil
	case m[d.Unit].pos < m[d.Target].pos:
		for _, sub := range m {
			if sub.pos <= m[d.Target].pos &&
				sub.pos > m[d.Unit].pos {
				modifier = modifier * sub.delta
			}
		}
		return (d.Value / modifier), nil
	case m[d.Unit].pos > m[d.Target].pos:
		for _, sub := range m {
			if sub.pos > m[d.Target].pos &&
				sub.pos <= m[d.Unit].pos {
				modifier = modifier * sub.delta
			}
		}
		return (d.Value * modifier), nil
	default:
		return 0, errors.New(invalidConv)
	}

}
