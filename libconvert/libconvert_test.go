package libconvert_test

import (
	"testing"

	"example.com/convert/libconvert"
)

type Data struct {
	Unit   string
	Value  float64
	Target string
	Conv   string
}

func TestTempConv(t *testing.T) {
	// TODO

	tempTables := []struct {
		Data
		out float64
	}{
		{Data{"C", 100, "F", "temp"}, 212},
		{Data{"C", 213.504, "F", "temp"}, 416.3072},
		{Data{"C", 200, "K", "temp"}, 473.15},

		{Data{"F", 301, "C", "temp"}, 149.444444},
		{Data{"F", 425.1231, "C", "temp"}, 218.401722},
		{Data{"F", 600, "K", "temp"}, 588.705556},

		{Data{"K", 200, "C", "temp"}, -73.15},
		{Data{"K", 231.444, "C", "temp"}, -41.70600},
		{Data{"K", 231.444, "F", "temp"}, -43.0708},
	}

	for _, sub := range tempTables {
		results, _ := libconvert.Data(sub.Data).TempConv()
		if results != sub.out {
			t.Errorf("Convert %s to %s, received: %v, want %v", sub.Unit, sub.Target, results, sub.out)
		}
	}

	lengthTables := []struct {
		Data
		out   float64
		ctype string
	}{
		{Data{"mm", 1, "cm", "metric"}, 0.1, "metr"},
		{Data{"mm", 1, "m", "metric"}, 0.001, "metr"},
		{Data{"km", 1, "mm", "metric"}, 1000000, "metr"},
		{Data{"m", 1, "cm", "metric"}, 100, "metr"},

		{Data{"in", 1, "mm", "metric"}, 0, "metr"},
		{Data{"aa", 1, "cm", "metric"}, 0, "metr"},
		{Data{"in", 1, "ft", "metric"}, 0, "metr"},
		{Data{"aa", 1, "ii", "metric"}, 0, "metr"},

		{Data{"in", 12, "ft", "imperial"}, 1, "imper"},
		{Data{"yd", 1.5, "in", "imperial"}, 54, "imper"},
		{Data{"yd", 1.5, "ft", "imperial"}, 4.5, "imper"},

		{Data{"ft", 1, "ft", "mixlength"}, 0, "mix"},
		{Data{"yd", 1.5, "in", "imperial"}, 0, "mix"},
		{Data{"yd", 1.5, "ft", "metric"}, 0, "mix"},
		{Data{"sf", 1.5, "ft", "imperial"}, 0, "mix"},
		{Data{"sf", 1.5, "mm", "mixlength"}, 0, "mix"},

		// {Data{"mm", 1, "cm"}, 0, "mix"},
		// {Data{"mm", 1, "m"}, 0, "mix"},
		// {Data{"km", 1, "mm"}, 0, "mix"},
		// {Data{"m", 1, "cm"}, 0, "mix"},
	}

	for _, sub := range lengthTables {
		var results float64

		switch {
		case sub.ctype == "metr":
			results, _ = libconvert.Data(sub.Data).MetrConv()
		case sub.ctype == "imper":
			results, _ = libconvert.Data(sub.Data).ImperConv()
		case sub.ctype == "mix":
			results, _ = libconvert.Data(sub.Data).ImperToMetr()
		}

		if results != sub.out {
			t.Errorf("Convert %s to %s, received: %v, want %v", sub.Unit, sub.Target, results, sub.out)
		}
	}

}
