package libconvert

import (
	"errors"
	"regexp"
	"strconv"
)

type Data struct {
	Unit   string
	Value  float64
	Target string
	Conv   string
}

var invalidConv string = "Invalid unit conversion"

// Returns the unit conversion type
func determineType(d Data) (string, error) {

	metric := []string{"mm", "cm", "m", "km"}
	imperial := []string{"in", "ft", "yd", "mi"}
	temp := []string{"C", "F", "K"}

	var unitType string
	var targetType string

	for _, m := range metric {
		if d.Unit == m {
			unitType = "metric"
		}
		if d.Target == m {
			targetType = "metric"
		}
	}

	for _, i := range imperial {
		if d.Unit == i {
			unitType = "imperial"
		}
		if d.Target == i {
			targetType = "imperial"
		}
	}

	for _, t := range temp {
		if d.Unit == t {
			unitType = "temp"
		}
		if d.Target == t {
			targetType = "temp"
		}
	}

	switch {
	case unitType == "" || targetType == "":
		return "", errors.New(invalidConv)
	case unitType == targetType:
		switch {
		case unitType == "temp":
			return "temp", nil
		case unitType == "metric":
			return "metric", nil
		case unitType == "imperial":
			return "imperial", nil
		}
	case unitType == "imperial" || unitType == "metric" && targetType == "imperial" || targetType == "metric":
		return "mixlength", nil
	}

	return "", errors.New(invalidConv)
}

// Returns struct into Data{} suitable for libconvert, returns an error for invalid units
func Format(input string, target string) (Data, error) {
	rexUnit := regexp.MustCompile(`[a-zA-Z]$|[a-zA-Z][a-z]$`)
	rexVal := regexp.MustCompile(`[-.0-9]+`)
	u := rexUnit.FindString(input)
	v := rexVal.FindString(input)
	vfloat, _ := strconv.ParseFloat(v, 64)

	if u == "" || v == "" {
		err := errors.New(invalidConv)
		return Data{}, err
	}

	d := Data{
		Unit:   u,
		Value:  vfloat,
		Target: target,
		Conv:   "",
	}

	t, err := determineType(d)

	if err != nil {
		return Data{}, err
	}

	d.Conv = t
	return d, nil
}

// Converts Between Temperature Units: Celsius, Fahrenheit or Kelvin
func (d Data) TempConv() (float64, error) {
	_, err := determineType(d)

	if err != nil {
		return 0, err
	}

	if d.Conv != "temp" {
		return 0, errors.New(invalidConv)
	}
	switch {
	case d.Unit == d.Target:
		return d.Value, nil
	case d.Unit == "C":
		switch {
		case d.Target == "F":
			return (d.Value*9/5 + 32), nil
		case d.Target == "K":
			return (d.Value + 273.15), nil
		}
	case d.Unit == "F":
		switch {
		case d.Target == "C":
			return ((d.Value - 32) * 5 / 9), nil
		case d.Target == "K":
			return ((d.Value-32)*5/9 + 273.15), nil
		}
	case d.Unit == "K":
		switch {
		case d.Target == "C":
			return (d.Value - 273.15), nil
		case d.Target == "F":
			return ((d.Value-273.15)*9/5 + 32), nil
		}
	}
	return 0, errors.New(invalidConv)
}

// These two functions are wrappers to private functions
// This is because .ImperToMetr() needs to call
// both metrConv() and imperConv()

// Converts between Metric units
func (d Data) MetrConv() (float64, error) {
	return metrConv(d)
}

// Converts between Imperial values
func (d Data) ImperConv() (float64, error) {
	return imperConv(d)
}

// Converts Imperial values to/from Metric Units
func (d Data) ImperToMetr() (float64, error) {
	if d.Conv != "mixlength" {
		return 0, errors.New(invalidConv)
	}
	_, err := determineType(d)
	if err != nil {
		return 0, err
	}

	var targetType string
	metric := []string{"mm", "cm", "m", "km"}
	imperial := []string{"in", "ft", "yd", "mi"}

	for _, m := range metric {
		if d.Target == m {
			targetType = "metric"
			break
		}
	}
	for _, i := range imperial {
		if d.Target == i {
			targetType = "imperial"
			break
		}
	}

	switch {
	case targetType == "metric":
		valInInch := d.Value
		if d.Unit != "in" {
			valInInch, _ = imperConv(Data{d.Unit, d.Value, "in", "imperial"})
		}
		return metrConv(Data{"mm", valInInch / 0.0039370, d.Target, "metric"})
	case targetType == "imperial":
		valInMm := d.Value
		if d.Unit != "mm" {
			valInMm, _ = metrConv(Data{d.Unit, d.Value, "mm", "metric"})
		}
		return imperConv(Data{"in", valInMm * 0.0039370, d.Target, "metric"})
	default:
		return 0, errors.New(invalidConv)
	}

}

// Converts any combination of two Metric/Imperial units
func (d Data) LengthConv() (float64, error) {
	_, err := determineType(d)

	if err != nil {
		return 0, err
	}

	switch {
	case d.Conv == "temp":
		return d.TempConv()
	case d.Conv == "metric":
		return d.MetrConv()
	case d.Conv == "imperial":
		return d.ImperConv()
	case d.Conv == "mixlength":
		return d.ImperToMetr()
	default:
		return 0, errors.New(invalidConv)
	}
}
