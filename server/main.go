package server

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"example.com/convert/libconvert"
)

func apiHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}

	f := r.Form
	d, err := libconvert.Format(
		strings.Join(f["query"], ""),
		strings.Join(f["target"], ""),
	)

	var p string

	switch {
	case f["type"][0] == "temp":
		r, err := d.TempConv()
		if err == nil {
			p = fmt.Sprintf("%g°%s", r, d.Target)
		}
	case f["type"][0] == "length":
		r, err := d.LengthConv()
		if err == nil {
			fmt.Printf("%v%s", r, d.Target)
		}
	default:
		err = errors.New("invalid conversion")
	}

	if err != nil {
		e := fmt.Sprintf(`{ "data": "", "error": "%s"}`, err.Error())
		w.Write([]byte(e))
	}

	s := fmt.Sprintf(`{"data": "%s, "error": ""}`, p)
	w.Write([]byte(s))

}

func ListenAndServe(port int) error {
	fmt.Printf("server running on port :%v\n", port)
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/api/", apiHandler)

	if err := http.ListenAndServe(":"+strconv.Itoa(port), nil); err != nil {
		return err
	}

	return nil

}
