package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"example.com/convert/libconvert"
	"example.com/convert/server"
)

func main() {

	ftype := flag.String("type", "", "Either temperature or length (temp/length)")
	finput := flag.String("input", "", "The value to convert: a number ending with their type, (e.g 100C or 10mi)")
	ftarget := flag.String("target", "", "Target format to convert to: (C/F/K) or (mm/cm/m/km/in/ft/yd/mi)")
	fserver := flag.Bool("server", false, "Serve convert as an API")
	fport := flag.Int("port", 9000, "Specify the port to serve the API from")
	flag.Parse()

	if *fserver {
		log.Fatal(server.ListenAndServe(*fport))
	}

	switch {
	case *ftype == "":
		log.Fatal("Missing required -type flag. Conversion type was not provided (temp/length)")
	case *finput == "":
		log.Fatal("Missing required -input flag. Input value was not provided (e.g 100C or 10mi")
	case *ftarget == "":
		log.Fatal("Missing required -target flag. Target unit was not provided (C/F/K) or (mm/cm/m/km/in/ft/yd/mi)")
	}

	d, err := libconvert.Format(*finput, *ftarget)

	if err != nil {
		log.Fatal(err)
	}

	switch {
	case *ftype == "temp":
		r, err := d.TempConv()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%g°%s", r, d.Target)
	case *ftype == "length":
		r, err := d.LengthConv()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v%s", r, d.Target)
	}

	os.Exit(0)
}
