# Convert
Een basis CLI temperatuur en lengte app. Gemaakt in Go.

## Building
``go build .``

## Running
``./convert -input 50.6mm -target in -type length``

